const rewireHotLoader = require('react-app-rewire-hot-loader');
const rewireGqlTag = require('react-app-rewire-graphql-tag')

module.exports = function override(config, env) {
    config = rewireGqlTag(rewireHotLoader(config, env), env);
    return config;
}
