import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { SchemaLink } from 'apollo-link-schema';

import { schema } from 'gql/schema';


export const client = new ApolloClient({
    cache: new InMemoryCache(window.__APOLLO_STATE__),
    link: new SchemaLink({ schema })
});
