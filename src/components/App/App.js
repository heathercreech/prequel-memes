import React from 'react';
import { graphql } from 'graphql';
import { Router } from '@reach/router';
import GraphiQL from 'graphiql';
import { schema } from 'gql/schema';
import { Posts } from 'components/Posts';
import { Header } from 'components/Header';
import { SubredditProvider } from 'components/SubredditProvider';

const QueryArea = () => (
    <div style={{ height: '100vh' }}>
        <GraphiQL
            schema={ schema }
            fetcher={ params => graphql(schema, params.query) }
        />
    </div>
);



export const App = () => (
    <div className='App'>
        <SubredditProvider>
            <Header />
            <div style={{ marginTop: '80px' }}>
                <Router>
                    <Posts path='/' />
                    <QueryArea path='/graphql' />
                </Router>
            </div>
        </SubredditProvider>
    </div>
);
