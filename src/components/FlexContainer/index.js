import styled from 'styled-components';


export const FlexContainer = styled.div`
    font-size: large;
    display: grid;
    grid-gap: 30px;
    padding: 0 20px;
    grid-template-columns: repeat(1, auto);
    @media only screen and (min-width: 768px) {
        grid-template-columns: repeat(3, auto);
    }
`;
