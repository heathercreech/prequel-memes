import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

import { SubredditConsumer } from 'components/SubredditProvider';



export const Header = () => (
    <AppBar>
        <Toolbar>
            <div style={{ flexGrow: 1 }} />
            <FormControl style={{ display: 'inline-block' }}>
                <FormLabel style={{ 'margin-right': '30px' }}>
                    Subreddit
                </FormLabel>
                <SubredditConsumer>
                {({ url, updateSubreddit }) => (
                    <RadioGroup style={{ display: 'inline-block' }} value={ url } onChange={ e => updateSubreddit(e.target.value) }>
                        <FormControlLabel
                            aria-label='Prequel'
                            label='Prequel'
                            name='subreddit'
                            value='https://www.reddit.com/r/prequelmemes'
                            control={ <Radio /> }
                        />
                        <FormControlLabel
                            aria-label='Sequel'
                            label='Sequel'
                            name='subreddit'
                            value='https://www.reddit.com/r/sequelmemes'
                            control={ <Radio /> }
                        />
                    </RadioGroup>
                ) }
                </SubredditConsumer>
            </FormControl>
        </Toolbar>
    </AppBar>
);
