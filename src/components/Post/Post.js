import React from 'react';
import styled from 'styled-components';
/*
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { BooleanValue } from 'react-values';
*/
import { PostImage } from './PostImage';
import { PostVideo } from './PostVideo';


const PostSource = styled.img.attrs({
    src: ({ source }) => `img/${source}.svg`,
    alt: ({ source }) => source
})`
    z-index: -5;
    position: absolute;
    width: 50px;
    opacity: 0.1;
`;

const TitleBar = ({ title, source }) => (
    <div style={{ display: 'flex', justifyContent: 'center', alignContent: 'center', alignItems: 'center', position: 'relative' }}>
        <PostSource source={ source } />
        <h3>{ title }</h3>
    </div>
);

const PostWrapper = styled.div`
    //padding: 20px;
    position: relative;
`;

export const Post = ({ title, url, source, isVideo, videoUrl }) => (
    <PostWrapper>
        <TitleBar title={ title }  source={ source } />
        { (!isVideo)
            ? <PostImage url={ url } alt={ title } />
            : <PostVideo url={ videoUrl } />
        }
    </PostWrapper>
)


/*
const Comments = styled.div`
    overflow: auto;
    height: 100%;
    padding: 0 20px;
    box-shadow: inset 0 0 10px 1px black;
`;
const Author = styled.div`
    font-weight: 500;
    color: lightgrey;
    font-size: 12px;
    margin-bottom: 5px;
`;
const Body = styled.div`
    font-size: 16px;
    font-weight: 600;
    line-height: 1.5rem;
`;
const Comment = styled.div`
    padding: 20px 0;
    color: white;
`;
*/

const Button = styled.button`
    display: block;
    border: none;
    background: white;
    padding: 20px;
    font-size: 20px;
    font-weight: 600;
    cursor: pointer;
    top: 50%;
    position: relative;
    margin: auto;
    letter-spacing: 0.1em;
    &:hover {
        background: none;
        border: 1px solid white;
        color: white;
    }
`;
export const PostOverlay = styled.div.attrs({
    tabIndex: 0
})`
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    opacity: 0;
    z-index: 1;
    &:hover, &:focus, ${Button}:focus {
        opacity: 1;
        background: rgba(0,0,0,.9);
    }
`;


const PostMediaWrapper = styled.div`
    position: relative;
    width: fit-content;
`;

export const PostRedux = ({ title, url, imageUrl, source, isVideo, videoUrl }) => (
    <PostWrapper>
        <PostWrapper>
            <TitleBar title={ title }  source={ source } />
            <PostMediaWrapper>
                { (!isVideo)
                    ? <PostImage url={ imageUrl } alt={ title } />
                    : <PostVideo url={ videoUrl } />
                }
                {/*
                <PostOverlay>
                    <BooleanValue>
                    {({ value, toggle }) => (
                        <div style={{ height: '100%' }}>
                            { !value && <Button onClick={ toggle }>Load Comments</Button> }
                            { value && (
                                <Query query={ gql`{ post(url: "${ url }") { comments { id author body } } }` }>
                                {({ loading, error, data }) => !loading && !error && (
                                    <Comments>
                                        { data.post.comments.map(comment => (
                                            <Comment
                                                key={ comment.id }
                                            >
                                                <Author>{ comment.author }</Author>
                                                <Body>{ comment.body }</Body>
                                            </Comment>
                                        )) }
                                    </Comments>
                                )}
                                </Query>
                            ) }
                        </div>
                    )}
                    </BooleanValue>
                </PostOverlay>
                */}
            </PostMediaWrapper>
        </PostWrapper>
    </PostWrapper>
);
