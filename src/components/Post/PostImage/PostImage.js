import styled from 'styled-components';

export const PostImage = styled.img.attrs({
    src: ({ url }) => !url.includes('imgur') ? url : `${url}.png`,
    alt: ({ alt }) => alt
})`
    width: 100%;
    height: auto;
`