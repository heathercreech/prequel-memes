import React from 'react';
import Video from 'react-player';


export const PostVideo = ({ url }) => (
    <Video
        url={ url }
        playing
        loop
        muted
        width='100%'
        height='auto'
    />
)