import React from 'react';
import { Query } from 'react-apollo';
import InfiniteScroll from 'react-infinite-scroller';
import { FlexContainer } from 'components/FlexContainer';
import { PostRedux } from 'components/Post/Post';
import { SubredditConsumer } from 'components/SubredditProvider';
import PostsQuery from './PostsQuery.gql';


export const Posts = () => (
    <SubredditConsumer>
    {({ url }) => (
        <Query
            query={ PostsQuery }
            variables={{ url }}
        >
            {({ loading, error, data, fetchMore }) => !loading && !error && (
                <InfiniteScroll
                    initialLoad={ false }
                    loadMore={ () => fetchMore({
                        query: PostsQuery,
                        variables: { url, after: data.posts.endCursor },
                        updateQuery: (previousResult, { fetchMoreResult }) => {
                            let previousPosts = previousResult.posts.edges;
                            let additionalPosts = fetchMoreResult.posts.edges;
                            let updatedCursor = fetchMoreResult.posts.endCursor;

                            return {
                                posts: {
                                    endCursor: updatedCursor,
                                    edges: [...previousPosts, ...additionalPosts],
                                    __typename: previousResult.posts.__typename
                                }
                            }
                        }
                    }) }
                    hasMore={ true }
                >
                    <FlexContainer>
                        { data.posts.edges.map(post => (
                            <PostRedux
                                key={ post.title }
                                title={ post.title }
                                url={ post.url }
                                imageUrl={ post.imageUrl }
                            />
                        )) }
                    </FlexContainer>
                </InfiniteScroll>
            )}
        </Query>
    ) }
    </SubredditConsumer>
);
