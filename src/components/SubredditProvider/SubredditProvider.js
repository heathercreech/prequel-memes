import React, { Component } from 'react';


const Context = React.createContext();
export const SubredditConsumer = Context.Consumer;

export class SubredditProvider extends Component {
    state = {
        url: 'https://www.reddit.com/r/prequelmemes'
    }

    updateSubreddit = url => this.setState({ url })

    render() {
        return (
            <Context.Provider value={{ ...this.state, updateSubreddit: this.updateSubreddit }}>
                { this.props.children }
            </Context.Provider>
        );
    }
}
