import { schema } from './schema';
import { graphql } from 'graphql';
import '../../node_modules/graphiql/dist/app.css';
import '../../node_modules/graphiql/dist/history.css';
import '../../node_modules/graphiql/dist/info.css';
import '../../node_modules/graphiql/dist/loading.css';
import '../../node_modules/graphiql/dist/doc-explorer.css';
import '../../node_modules/graphiql/dist/codemirror.css';
import '../../node_modules/graphiql/dist/foldgutter.css';
import '../../node_modules/graphiql/dist/lint.css';
import '../../node_modules/graphiql/dist/show-hint.css';
import '../../node_modules/graphiql/dist/jump.css';

const datapoints = 'id, title, url, stickied, source, isVideo, videoUrl';


export const prequel_query = (after) => graphql(
    schema,
    `{
        posts(
            after: "t3_${after}",
            source: "prequel",
            url: "https://www.reddit.com/r/prequelmemes"
        ) {
            ${datapoints}
        }
    }`
);


export const sequel_query = (after) => graphql(
    schema,
    `{
        posts(
            after: "t3_${after}",
            source: "sequel",
            url: "https://www.reddit.com/r/sequelmemes"
        ) {
            ${datapoints}
        }
    }`
);


export { schema };
