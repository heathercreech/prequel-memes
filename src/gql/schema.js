import { makeExecutableSchema } from 'graphql-tools';

const typeDefs = `
    
    type Comment {
        id: ID!
        author: String!
        upvotes: Int
        body: String
        replies: [Comment]!
    }

    type Post {
        id: String!
        title: String!
        url: String!
        author: String!
        flair: String
        source: String
        imageUrl: String
        videoUrl: String
        isVideo: Boolean
        stickied: Boolean
        comments: [Comment]!
    }

    type PostConnection {
        edges: [Post]!
        endCursor: String
    }

    type Query {
        posts(after: String, url: String, source: String): PostConnection
        post(url: String!): Post
    }
`;



const resolvers = {
    Query: {
        posts: (obj, { after, url, source }) => {
            return fetch(`${url}.json?after=${after}`).then(
                res => res.json()
            ).then(
                json => ({
                    edges: json.data.children.map(child => Object.assign({ source }, child.data)).filter(post => post.stickied === false && post.pinned === false),
                    endCursor: json.data.after
                })
            );
        },
        post: (obj, { url }) => fetch(`${ url }.json`)
            .then(res => res.json())
            .then(json => json[0].data.children[0].data)
    },
    Post: {
        //url: post => `https://www.reddit.com${ post.permalink.substring(0, post.permalink.length - 1) }`,
        comments: post => fetch(
                `https://www.reddit.com${ post.permalink.substring(0, post.permalink.length - 1) }.json`
            ).then(res => res.json())
        .then(json => json.slice(1)[0].data.children.filter(item => item.kind === 't1')),
        url: post => `https://www.reddit.com${ post.permalink.substring(0, post.permalink.length - 1) }`,
        imageUrl: post => post.url,
        flair: (post) => post.link_flair_text,
        isVideo: ({ is_video, post_hint }) => post_hint.includes('video'),
        videoUrl: ({ media, post_hint, url }) => {
            if (post_hint === 'hosted:video') return media.reddit_video.fallback_url;
            if (post_hint === 'rich:video') return url;
            return undefined;
        },
    },
    Comment: {
        id: comment => comment.data.id,
        body: comment => comment.data.body,
        author: comment => comment.data.author,
        upvotes: comment => comment.data.ups,
        replies: comment => comment.data.replies ? comment.data.replies.data.children.filter(item => item.kind === 't1') : []
    }
};

export const schema = makeExecutableSchema({ typeDefs, resolvers });
