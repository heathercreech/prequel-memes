import React from 'react';
import ReactDOM from 'react-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import './index.css';
import { ApolloProvider } from 'react-apollo';
import { App } from 'components/App';
import { AppContainer } from 'react-hot-loader';
import { client } from 'apollo/client';
import theme from 'theme';
import registerServiceWorker from './registerServiceWorker';


const render = Component => {
    ReactDOM.render(
        <AppContainer>
            <ApolloProvider client={ client }>
                <MuiThemeProvider theme={ theme }>
                    <Component />
                </MuiThemeProvider>
            </ApolloProvider>
        </AppContainer>,
        document.getElementById('root')
    );
}
registerServiceWorker();

render(App)
if (module.hot) {
    module.hot.accept('components/App', () => { render(App) });
}
